package ru.dz.database.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.telegram.telegrambots.api.objects.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "telegram_user")
public class TUser implements Serializable {
    @Id
    private Integer id;

    @Column(name = "vote_status")
    private Boolean voteStatus;


    @JsonBackReference
    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy = "user")
    private List<Image> images;


    public TUser(User user) {
        this.id = user.getId();
        this.voteStatus = false;
    }
}
