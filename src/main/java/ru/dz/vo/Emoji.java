package ru.dz.vo;

public enum Emoji {
    START("▶", null),
    STOP("\u23F9", null),
    TOP("\uD83D", "\uDD1D"),
    RATING("\uD83D", "\uDCCA"),
    HELP("\uD83C", "\uDD98"),
    DELETE("❌", null),
    ONE("1️⃣", null),
    TWO("2️⃣", null),
    THREE("3️⃣", null),
    FOUR("4️⃣", null),
    FIVE("5️⃣", null);

    String firstChar;
    String secondChar;

    Emoji(String firstChar, String secondChar) {
        this.firstChar = firstChar;
        this.secondChar = secondChar;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (this.firstChar != null) {
            sb.append(this.firstChar);
        }
        if (this.secondChar != null) {
            sb.append(this.secondChar);
        }
        return sb.toString();
    }
}
