package ru.dz.services;

import org.hibernate.Session;
import org.telegram.telegrambots.api.objects.User;
import ru.dz.database.HibernateUtil;
import ru.dz.database.entities.TUser;

public class UsersService {
    private void registerUser(User user) {
        Session session = null;
        TUser tUser = new TUser(user);
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(tUser);
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session != null && session.getTransaction() != null)
                session.getTransaction().rollback();
            System.err.println("Error in registerUser(): " + e.getMessage());
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
    }

    public TUser getUser(User user) {
        TUser byId = getById(user.getId());
        if (byId != null) {
            return byId;
        } else {
            registerUser(user);
            return getById(user.getId());
        }
    }

    private TUser getById(Integer id) {
        Session session = null;
        TUser user = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            user = session.get(TUser.class, id); // создаем критерий запроса
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session != null && session.getTransaction() != null)
                session.getTransaction().rollback();
            System.err.println("Error in getById(): " + e.getMessage());
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
        return user;
    }

    public Boolean getVoteStatus(User user) {
        TUser byId = getById(user.getId());
        if (byId != null)
            return byId.getVoteStatus();
        else {
            registerUser(user);
            return false;
        }
    }

    public void updateStatus(Integer id, Boolean status) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.createQuery("update TUser u set u.voteStatus = :newStatus where u.id = :id")
                    .setParameter("newStatus", status)
                    .setParameter("id", id)
                    .executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session != null && session.getTransaction() != null)
                session.getTransaction().rollback();
            System.err.println("Error in updateStatus(): " + e.getMessage());
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
    }
}
