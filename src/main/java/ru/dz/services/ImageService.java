package ru.dz.services;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import ru.dz.database.HibernateUtil;
import ru.dz.database.entities.Image;
import ru.dz.database.entities.TUser;

import java.util.List;

public class ImageService {
    public Image getImageById(String id) {
        Session session = null;
        Image image = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            image = session.get(Image.class, id); // создаем критерий запроса
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session != null && session.getTransaction() != null)
                session.getTransaction().rollback();
            System.err.println("Error in getImageById(): " + e.getMessage());
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
        return image;
    }

    public void saveImage(String id, TUser user) {
        Session session = null;
        Image image = new Image(id, null, 0, 0.0, user);
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(image);
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session != null && session.getTransaction() != null)
                session.getTransaction().rollback();
            System.err.println("Error in saveImage(): " + e.getMessage());
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
    }

    public Image getRandomPhoto() {
        Session session = null;
        Image image = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Long rows = (Long) session.createCriteria(Image.class).setProjection(Projections.rowCount()).uniqueResult();
            image = (Image) session.createSQLQuery("SELECT * from image OFFSET floor(random()*" + rows + ") LIMIT 1;")
                    .addEntity(Image.class)
                    .uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session != null && session.getTransaction() != null)
                session.getTransaction().rollback();
            System.err.println("Error in randomImage(): " + e.getMessage());
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
        return image;
    }

    public Image updateImage(Image imageById) {
        Session session = null;
        Image image = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(imageById);
            image = session.get(Image.class, imageById.getId());
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session != null && session.getTransaction() != null)
                session.getTransaction().rollback();
            System.err.println("Error in updateImage(): " + e.getMessage());
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
        return image;
    }

    public List<Image> getTopImages() {
        Session session = null;
        List<Image> images = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Image.class);
            criteria.addOrder(Order.desc("top"));
            criteria.setMaxResults(5);
            images = (List<Image>) criteria.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session != null && session.getTransaction() != null)
                session.getTransaction().rollback();
            System.err.println("Error in getTopImages(): " + e.getMessage());
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
        return images;
    }

    public void delete(String idToDelete) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.createQuery("delete from Image where id=:id")
                    .setParameter("id", idToDelete)
                    .executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session != null && session.getTransaction() != null)
                session.getTransaction().rollback();
            System.err.println("Error in deleteImage(): " + e.getMessage());
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
    }

    public List<Image> getMyImages(TUser user) {
        Session session = null;
        List<Image> images = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            images = (List<Image>) session.createQuery("from Image where user=:user")
                    .setParameter("user", user)
                    .list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session != null && session.getTransaction() != null)
                session.getTransaction().rollback();
            System.err.println("Error in getTopImages(): " + e.getMessage());
        } finally {
            if (session != null && session.isOpen())
                session.close();
        }
        return images;
    }
}
