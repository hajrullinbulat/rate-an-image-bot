package ru.dz.vo;

public enum Commands {
    START(Emoji.START.toString() + "Start"),
    STOP(Emoji.STOP.toString() + "Stop"),
    RATE(Emoji.RATING.toString() + "Rating"),
    TOP(Emoji.TOP.toString() + "Top5"),
    HELP(Emoji.HELP.toString() + "Help");

    final String command;

    Commands(String command) {
        this.command = command;
    }

    @Override
    public String toString() {
        return command;
    }
}
