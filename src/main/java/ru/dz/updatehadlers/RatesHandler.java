package ru.dz.updatehadlers;

import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageCaption;
import org.telegram.telegrambots.api.objects.*;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.logging.BotLogger;
import ru.dz.database.entities.Image;
import ru.dz.database.entities.TUser;
import ru.dz.services.ImageService;
import ru.dz.services.UsersService;
import ru.dz.vo.Commands;
import ru.dz.vo.Constants;
import ru.dz.vo.Emoji;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RatesHandler extends TelegramLongPollingBot {
    private static final UsersService usersService = new UsersService();
    private static final ImageService imageService = new ImageService();

    private static final String token = "242639122:AAG3nSUP7RKsbD04k3RGHBkqL03-tN2hqXw";
    private static final String name = "Bulatbotbot";

    @Override
    public void onUpdateReceived(Update update) {
        try {
            if (update.hasMessage()) {
                Message message = update.getMessage();
                if (message.hasText())
                    handleIncomingMessage(message);
                else {
                    List<PhotoSize> photo = message.getPhoto();
                    if (photo != null)
                        handleImage(message, photo);
                }
            }
            if (update.hasCallbackQuery())
                handleInlineButtonPressed(update.getCallbackQuery());
        } catch (Exception e) {
            BotLogger.error("error onUpdateReceived", e);
        }
    }

    private void handleInlineButtonPressed(CallbackQuery callBackQuery) throws TelegramApiException {
        String[] split = callBackQuery.getData().split(";");
        if (!getDeleteCommand().equals(split[0])) {
            inlineButtonToRatePressed(split[0], split[1], callBackQuery.getMessage());
        } else {
            inlineButtonToDeletePressed(split[1], callBackQuery.getMessage());
        }
    }

    private void handleIncomingMessage(Message message) throws TelegramApiException {
        if (message.hasText()) {
            String messageText = message.getText();
            if (getStartCommand().equals(messageText)) {
                onStartChoosen(message);
            } else if (getStopCommand().equals(messageText)) {
                onStopChoosen(message);
            } else if (getRateCommand().equals(messageText)) {
                onRatingChoosen(message);
            } else if (getTopCommand().equals(messageText)) {
                onTopChoosen(message);
            } else if (getHelpCommand().equals(messageText)) {
                onHelpChoosen(message);
            } else {
                sendMessageFactory(message, getMainKeyboard(), getHelpText(), false);
            }
        } else {
            sendMessageFactory(message, getMainKeyboard(), getHelpText(), false);
        }
    }

    private void handleImage(Message message, List<PhotoSize> photo) throws TelegramApiException {
        TUser byId = usersService.getUser(message.getFrom());
        String fileId = photo.get(0).getFileId();
        imageService.saveImage(fileId, byId);
        sendMessageFactory(message, getMainKeyboard(), getImageUploadedText(), true);
    }

    private void onStartChoosen(Message message) throws TelegramApiException {
        User from = message.getFrom();
        if (!usersService.getVoteStatus(from))
            usersService.updateStatus(from.getId(), true);
        sendImageAnswer(message);
    }

    private void onStopChoosen(Message message) throws TelegramApiException {
        User from = message.getFrom();
        if (usersService.getVoteStatus(from))
            usersService.updateStatus(from.getId(), false);
        sendMessageFactory(message, getMainKeyboard(), getOnStopChoosenText(), true);
    }

    private void onTopChoosen(Message message) throws TelegramApiException {
        List<Image> topImages = imageService.getTopImages();
        if (checkOfNullAndNotAZeroElem(topImages)) {
            sendTopImages(topImages, message);
        } else {
            sendMessageFactory(message, getMainKeyboard(), getNoPhotoText(), true);
        }
    }

    private void onRatingChoosen(Message message) throws TelegramApiException {
        TUser user = usersService.getUser(message.getFrom());
        List<Image> myImages = imageService.getMyImages(user);
        if (checkOfNullAndNotAZeroElem(myImages))
            myImages.forEach(image -> {
                sendPhotoFactory(message, image,
                        getImageCaption(image, Constants.nobodyRated),
                        getInlineDeleteKeyboard(image.getId()), true);
            });
        else
            sendMessageFactory(message, getMainKeyboard(), getNoUploadedPhotoText(), true);
    }

    private void onHelpChoosen(Message message) throws TelegramApiException {
        sendMessageFactory(message, getMainKeyboard(), getHelpText(), true);
    }


    private void inlineButtonToDeletePressed(String idToDelete, Message message) throws TelegramApiException {
        imageService.delete(idToDelete);
        editMessageAfterDelete(message);
    }

    private void inlineButtonToRatePressed(String rate, String id, Message message) throws TelegramApiException {
        Image imageById = imageService.getImageById(id);
        Integer amount = imageById.getAmount();
        double rating = Double.parseDouble(rate);
        if (amount != 0) {
            Integer newAmount = amount + 1;
            Double newRating = imageById.getRating() + rating;
            imageById.setAmount(newAmount);
            imageById.setRating(newRating);
            imageById.setTop(newAmount * newRating);
        } else {
            imageById.setRating(rating);
            imageById.setAmount(1);
            imageById.setTop(rating);
        }
        Image image = imageService.updateImage(imageById);
        editMessageAfterRate(message, image.getAmount(), image.getRating());
        sendImageAnswer(message);
    }

    private void editMessageAfterRate(Message message, Integer newAmount, Double newRating) throws TelegramApiException {
        EditMessageCaption editMessageCaption = new EditMessageCaption();
        editMessageCaption.setChatId(message.getChatId().toString());
        editMessageCaption.setMessageId(message.getMessageId());
        editMessageCaption.setCaption(getImageCaptionRating(newAmount, newRating));
        editMessageCaption(editMessageCaption);
    }

    private void editMessageAfterDelete(Message message) throws TelegramApiException {
        EditMessageCaption editMessageCaption = new EditMessageCaption();
        editMessageCaption.setCaption(Constants.deleted);
        editMessageCaption.setChatId(message.getChatId().toString());
        editMessageCaption.setMessageId(message.getMessageId());
        editMessageCaption(editMessageCaption);
    }

    private void sendTopImages(List<Image> topImages, Message message) throws TelegramApiException {
        for (int pos = 0; pos < topImages.size(); pos++)
            sendTopImage(topImages.get(pos), pos + 1, message);
    }

    private void sendTopImage(Image topImage, int pos, Message message) throws TelegramApiException {
        sendPhotoFactory(message, topImage, getTopImageCaption(pos), getMainKeyboard(), true);
    }

    private String getTopImageCaption(int pos) {
        return "#" + pos;
    }

    private void sendPhotoFactory(Message message, Image image, String caption, ReplyKeyboard keyBoard, boolean withReplyToMessage) {
        try {
            SendPhoto sendPhoto = new SendPhoto();
            sendPhoto.setChatId(message.getChatId().toString());
            sendPhoto.setReplyMarkup(keyBoard);
            sendPhoto.setPhoto(image.getId());
            if (withReplyToMessage)
                sendPhoto.setReplyToMessageId(message.getMessageId());
            sendPhoto.setCaption(caption);
            sendPhoto(sendPhoto);
        } catch (TelegramApiException e) {
            BotLogger.error("error in sendPhotoFactory: ", e);
        }
    }

    private void sendImageAnswer(Message message) throws TelegramApiException {
        Image image = imageService.getRandomPhoto();
        if (image != null)
            sendPhotoFactory(message, image, getImageCaption(image, Constants.firstRate), getInlineRateKeyboard(image.getId()), false);
        else
            sendMessageFactory(message, getMainKeyboard(), getNoPhotoText(), true);
    }

    private ReplyKeyboard getInlineRateKeyboard(String photo_id) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> lists = new ArrayList<>();

        List<InlineKeyboardButton> list = new ArrayList<>();

        InlineKeyboardButton inlineKeyboardButton1 = new InlineKeyboardButton();
        inlineKeyboardButton1.setText(Emoji.ONE.toString());
        inlineKeyboardButton1.setCallbackData("1;" + photo_id);
        InlineKeyboardButton inlineKeyboardButton2 = new InlineKeyboardButton();
        inlineKeyboardButton2.setText(Emoji.TWO.toString());
        inlineKeyboardButton2.setCallbackData("2;" + photo_id);
        InlineKeyboardButton inlineKeyboardButton3 = new InlineKeyboardButton();
        inlineKeyboardButton3.setText(Emoji.THREE.toString());
        inlineKeyboardButton3.setCallbackData("3;" + photo_id);
        InlineKeyboardButton inlineKeyboardButton4 = new InlineKeyboardButton();
        inlineKeyboardButton4.setText(Emoji.FOUR.toString());
        inlineKeyboardButton4.setCallbackData("4;" + photo_id);
        InlineKeyboardButton inlineKeyboardButton5 = new InlineKeyboardButton();
        inlineKeyboardButton5.setText(Emoji.FIVE.toString());
        inlineKeyboardButton5.setCallbackData("5;" + photo_id);

        list.add(inlineKeyboardButton1);
        list.add(inlineKeyboardButton2);
        list.add(inlineKeyboardButton3);
        list.add(inlineKeyboardButton4);
        list.add(inlineKeyboardButton5);

        lists.add(list);

        inlineKeyboardMarkup.setKeyboard(lists);
        return inlineKeyboardMarkup;
    }

    private ReplyKeyboard getInlineDeleteKeyboard(String imageId) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> lists = new ArrayList<>();
        List<InlineKeyboardButton> list = new ArrayList<>();
        InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
        inlineKeyboardButton.setText(Emoji.DELETE.toString());
        inlineKeyboardButton.setCallbackData(Constants.deleteCommand + ";" + imageId);
        list.add(inlineKeyboardButton);
        lists.add(list);
        inlineKeyboardMarkup.setKeyboard(lists);
        return inlineKeyboardMarkup;
    }

    private void sendMessageFactory(Message message, ReplyKeyboardMarkup keyBoard, String text, boolean withReplyToMessage) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setReplyMarkup(keyBoard);
        if (withReplyToMessage)
            sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);
        sendMessage(sendMessage);
    }

    private ReplyKeyboardMarkup getMainKeyboard() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboad(false);

        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(getStartCommand());
        keyboardFirstRow.add(getStopCommand());
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardSecondRow.add(getRateCommand());
        keyboardSecondRow.add(getTopCommand());
        KeyboardRow keyboardThirdRow = new KeyboardRow();
        keyboardThirdRow.add(getHelpCommand());
        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);
        keyboard.add(keyboardThirdRow);

        replyKeyboardMarkup.setKeyboard(keyboard);
        return replyKeyboardMarkup;
    }

    private String getStopCommand() {
        return Commands.STOP.toString();
    }

    private String getRateCommand() {
        return Commands.RATE.toString();
    }

    private String getTopCommand() {
        return Commands.TOP.toString();
    }

    private String getStartCommand() {
        return Commands.START.toString();
    }

    private String getHelpCommand() {
        return Commands.HELP.toString();
    }

    private String getDeleteCommand() {
        return Constants.deleteCommand;
    }


    private String getNoUploadedPhotoText() {
        return Constants.noUploadedPhoto;
    }

    private String getNoPhotoText() {
        return Constants.noPhotoText;
    }

    private String getImageUploadedText() {
        return Constants.imageUploaded;
    }

    private String getHelpText() {
        return Emoji.HELP.toString() + Constants.helpHelp +
                Emoji.START.toString() + Constants.helpStart +
                Emoji.STOP.toString() + Constants.helpStop +
                Emoji.TOP.toString() + Constants.helpTop +
                Emoji.RATING.toString() + Constants.helpRate;
    }

    private String getOnStopChoosenText() {
        return Constants.onStopChoosen;
    }

    private String getTextWhenImageDeleted() {
        return Constants.imageDeleted;
    }

    private String getImageCaption(Image image, String text) {
        return image.getAmount() == 0 ? text : getImageCaptionRating(image.getAmount(), image.getRating());
    }

    private String getImageCaptionRating(int amount, double rating) {
        return "Проголосовавших: " +
                amount +
                ". Рейтинг: " +
                new BigDecimal
                        (rating / amount)
                        .setScale(1, BigDecimal.ROUND_UP)
                        .doubleValue() +
                ".";
    }

    private boolean checkOfNullAndNotAZeroElem(List list) {
        return list != null && list.size() > 0;
    }


    @Override
    public String getBotUsername() {
        return name;
    }

    @Override
    public String getBotToken() {
        return token;
    }
}
