import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.logging.BotLogger;
import ru.dz.updatehadlers.RatesHandler;
import ru.dz.updatehadlers.TestHandler;

public class HelloBotApp {
    public static void main(String[] args) {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new RatesHandler());
            telegramBotsApi.registerBot(new TestHandler());
        } catch (TelegramApiException e) {
            BotLogger.error("error", e);
        }
    }
}
